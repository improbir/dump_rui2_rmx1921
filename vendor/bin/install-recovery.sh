#!/vendor/bin/sh
if ! applypatch --check EMMC:/dev/block/bootdevice/by-name/recovery:67108864:61a343fa62b3950a8203b3814f29d4894174a4b2; then
  applypatch  \
          --patch /vendor/recovery-from-boot.p \
          --source EMMC:/dev/block/bootdevice/by-name/boot:67108864:986c80aa64e4b5b56b4d9552c370176f500c09a0 \
          --target EMMC:/dev/block/bootdevice/by-name/recovery:67108864:61a343fa62b3950a8203b3814f29d4894174a4b2 && \
      log -t recovery "Installing new oppo recovery image: succeeded" && \
      setprop ro.boot.recovery.updated true || \
      log -t recovery "Installing new oppo recovery image: failed" && \
      setprop ro.boot.recovery.updated false
else
  log -t recovery "Recovery image already installed"
  setprop ro.boot.recovery.updated true
fi
